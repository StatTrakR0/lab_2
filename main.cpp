#include <iostream>

#include <cmath>

using namespace std;

int main ()

{


  double a, b, c;
  double Max, Min;
  cout << "Enter the first number: ";
  cin >> a;
  cout << "Enter the second number: ";
  cin >> b;
  cout << "Enter the third number: ";
  cin >> c;
  if (a >= b && a >= c) Max = a;
  else if (b >= a && b >= c) Max = b;
  else  Max = c;

  if (a <= b && a <= c) Min = a;
  else if (b <= a && b <= c) Min = b;
  else Min = c;
  cout << "Minimum number is: " << Min << endl;
  cout << "Maximum number is: " << Max << endl;

  return 0;
}
